package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.cardissuer.ImageUrl
import com.jaf.buyit.core.domain.cardissuer.Name
import com.jaf.buyit.infrastructure.cardissuer.SpyCardIssuerService
import org.junit.Test
import java.util.*

class GetCardIssuerForPaymentMethodTest {

    private lateinit var paymentId: String
    private lateinit var getCardIssuersForPayment: GetCardIssuersForPaymentMethod
    private lateinit var cardIssuers: List<CardIssuer>

    @Test
    fun getAllCardIssuersForPaymentMethod() {
        givenAvailableCardIssuersForPaymentMethod(VISA)

        whenGetCardIssuersForPayment()

        thenContainsCardIssuer(GALICIA)
    }

    private fun givenAvailableCardIssuersForPaymentMethod(paymentId: String) {
        this.paymentId = paymentId
        getCardIssuersForPayment = GetCardIssuersForPaymentMethod(SpyCardIssuerService(Arrays.asList(GALICIA)))
    }


    private fun whenGetCardIssuersForPayment() {
        cardIssuers = getCardIssuersForPayment.doAction(paymentId)
    }

    private fun thenContainsCardIssuer(cardIssuer: CardIssuer) {
        assert(cardIssuers.any { it.identifier() == cardIssuer.identifier() })
    }

    companion object {
        private val VISA = "visa"
        private val GALICIA = CardIssuer("1", Name("santander"), ImageUrl("ww.ww.jpg"))
    }
}


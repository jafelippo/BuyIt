package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.installment.Message
import com.jaf.buyit.infrastructure.installment.SpyInstallmentService
import org.junit.Test
import java.util.*

class GetInstallmentsTest {

    private lateinit var paymentId: String
    private lateinit var cardIssuerId: String
    private lateinit var getInstallments: GetInstallments
    private lateinit var installments: List<Installment>

    @Test
    fun getsAllInstallForPaymentAndCardIssuer() {
        givenAPaymentMethod(VISA)
        givenACardIssuer(GALICIA)

        whenGetInstallments()

        thenHasInstallments()
    }

    private fun givenACardIssuer(cardIssuerId: String) {
        this.cardIssuerId = cardIssuerId
    }

    private fun givenAPaymentMethod(paymentId: String) {
        this.paymentId = paymentId
    }

    private fun whenGetInstallments() {
        createAction()
        installments = getInstallments.doAction(GetInstallments.ActionData(paymentId, cardIssuerId))
    }

    private fun createAction() {
        val spyInstallments = Arrays.asList(Installment(Message("1 cuota de 200")))
        getInstallments = GetInstallments(SpyInstallmentService(spyInstallments))
    }

    private fun thenHasInstallments() {
        assert(installments.isNotEmpty())
    }

    companion object {
        private val VISA = "visa"
        private val GALICIA = "galicia"
    }
}
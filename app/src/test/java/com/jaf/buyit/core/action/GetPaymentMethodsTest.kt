package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.payment.Name
import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.core.domain.payment.ImageUrl
import com.jaf.buyit.infrastructure.payment.SpyPaymentMethodService
import org.junit.Test
import java.util.*

class GetPaymentMethodsTest {

    private lateinit var getPaymentMethods: GetPaymentMethods
    private lateinit var payments: List<PaymentMethod>

    @Test
    fun execute() {
        givenAPaymentMethod(VISA)

        whenGetPayments()

        thenContainsPaymentMethod(VISA)
    }

    private fun givenAPaymentMethod(paymentMethod: PaymentMethod) {
        getPaymentMethods = GetPaymentMethods(SpyPaymentMethodService(Arrays.asList(paymentMethod)))
    }

    private fun whenGetPayments() {
        payments = getPaymentMethods.doAction()
    }

    private fun thenContainsPaymentMethod(visaPaymentMethod: PaymentMethod) {
        assert(payments.any { it.identifier() == visaPaymentMethod.identifier() })
    }

    companion object {
        private val VISA = PaymentMethod("visa", Name("visa"), ImageUrl("url.fake.com"))
    }
}


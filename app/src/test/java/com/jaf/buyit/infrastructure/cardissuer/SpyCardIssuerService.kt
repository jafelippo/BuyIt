package com.jaf.buyit.infrastructure.cardissuer

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.cardissuer.CardIssuerService


class SpyCardIssuerService(private val cardIssuers: List<CardIssuer>) : CardIssuerService {

    override fun allFor(paymentId: String): List<CardIssuer> {
        return cardIssuers
    }
}
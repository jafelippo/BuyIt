package com.jaf.buyit.infrastructure.payment

import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.core.domain.payment.PaymentMethodService


class SpyPaymentMethodService(private val paymentMethods: List<PaymentMethod>): PaymentMethodService {

    override fun findAll(): List<PaymentMethod> = paymentMethods
}
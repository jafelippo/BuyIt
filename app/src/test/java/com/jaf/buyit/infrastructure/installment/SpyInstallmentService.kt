package com.jaf.buyit.infrastructure.installment

import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.installment.InstallmentService


class SpyInstallmentService(private val installments: List<Installment>) : InstallmentService {
    override fun allFor(paymentId: String, cardIssuerId: String): List<Installment> {
        return installments
    }
}
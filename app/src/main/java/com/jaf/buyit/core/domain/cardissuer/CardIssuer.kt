package com.jaf.buyit.core.domain.cardissuer


class CardIssuer(private val id: String, private val name: Name, private val imageUrl: ImageUrl) {

    fun identifier(): String  = id

    fun name(): Name = name

    fun imageUrl(): ImageUrl = imageUrl
}
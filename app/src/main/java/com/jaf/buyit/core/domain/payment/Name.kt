package com.jaf.buyit.core.domain.payment

data class Name(val value: String) {
    init {
        validateFields()
    }

    private fun validateFields() {
        require(!value.isEmpty()) { "Payment method name cannot be empty" }
    }
}
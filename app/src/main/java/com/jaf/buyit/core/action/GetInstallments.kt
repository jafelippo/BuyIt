package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.installment.InstallmentService
import io.reactivex.Single
import io.reactivex.Single.fromCallable


class GetInstallments(private val installmentService: InstallmentService) {

    fun doAction(actionData: ActionData): List<Installment> {
        return installmentService.allFor(actionData.paymentId, actionData.cardIssuerId)
    }

    fun build(actionData: ActionData): Single<List<Installment>> =
            fromCallable { doAction(actionData) }

    class ActionData(val paymentId: String, val cardIssuerId: String)
}
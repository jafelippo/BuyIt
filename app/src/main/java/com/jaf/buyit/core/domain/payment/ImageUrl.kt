package com.jaf.buyit.core.domain.payment


data class ImageUrl(val value: String)
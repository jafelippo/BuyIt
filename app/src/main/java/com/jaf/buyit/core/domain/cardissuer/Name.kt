package com.jaf.buyit.core.domain.cardissuer


data class Name(val value: String) {
    init {
        validateFields()
    }

    private fun validateFields() {
        require(!value.isEmpty()) { "Bank name cannot be empty" }
    }
}
package com.jaf.buyit.core.domain.cardissuer


interface CardIssuerService {

    fun allFor(paymentId: String): List<CardIssuer>
}
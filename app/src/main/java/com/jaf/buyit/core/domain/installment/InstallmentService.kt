package com.jaf.buyit.core.domain.installment


interface InstallmentService {

    fun allFor(paymentId: String, cardIssuerId: String): List<Installment>
}
package com.jaf.buyit.core.domain.payment



class PaymentMethod(private val id: String, private val name: Name, private val imageUrl: ImageUrl) {

    fun name(): Name = name

    fun imageUrl(): ImageUrl = imageUrl

    fun identifier(): String = id
}
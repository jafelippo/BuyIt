package com.jaf.buyit.core.domain.payment


interface PaymentMethodService {

    fun findAll(): List<PaymentMethod>
}
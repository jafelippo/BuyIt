package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.core.domain.payment.PaymentMethodService
import io.reactivex.Single
import io.reactivex.Single.fromCallable


class GetPaymentMethods(private val paymentMethodService: PaymentMethodService) {

    fun doAction(): List<PaymentMethod> {
        return paymentMethodService.findAll()
    }

    fun build(): Single<List<PaymentMethod>> = fromCallable { doAction() }
}
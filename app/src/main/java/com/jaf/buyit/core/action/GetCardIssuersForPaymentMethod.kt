package com.jaf.buyit.core.action

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.cardissuer.CardIssuerService
import io.reactivex.Single
import io.reactivex.Single.fromCallable


class GetCardIssuersForPaymentMethod(private val cardIssuerService: CardIssuerService) {

    fun doAction(paymentId: String): List<CardIssuer> {
        return cardIssuerService.allFor(paymentId)
    }

    fun build(paymentId: String): Single<List<CardIssuer>> = fromCallable { doAction(paymentId) }
}
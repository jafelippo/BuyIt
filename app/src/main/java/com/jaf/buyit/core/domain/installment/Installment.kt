package com.jaf.buyit.core.domain.installment


class Installment(private val message: Message) {

    fun message(): Message = message
}
package com.jaf.buyit.infrastructure.installment

import com.google.gson.annotations.SerializedName


data class InstallmentResponse(@SerializedName("payer_costs")val installmentItems: List<InstallmentItem>)
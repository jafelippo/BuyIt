package com.jaf.buyit.infrastructure

import com.jaf.buyit.infrastructure.cardissuer.CardIssuerItem
import com.jaf.buyit.infrastructure.installment.InstallmentResponse
import com.jaf.buyit.infrastructure.payment.PaymentMethodItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface PaymentClient {

    @GET("payment_methods")
    fun getPayments(@Query("public_key") publicKey: String): Call<List<PaymentMethodItem>>

    @GET("payment_methods/card_issuers")
    fun getCardIssuers(@Query("public_key") publicKey: String,
                       @Query("payment_method_id") paymentId: String): Call<List<CardIssuerItem>>

    @GET("payment_methods/installments")
    fun getInstallments(@Query("public_key") publicKey: String,
                       @Query("payment_method_id") paymentId: String,
                       @Query("issuer.id") cardIssuer: String): Call<List<InstallmentResponse>>
}
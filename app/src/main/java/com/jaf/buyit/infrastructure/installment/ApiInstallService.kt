package com.jaf.buyit.infrastructure.installment

import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.installment.InstallmentService
import com.jaf.buyit.infrastructure.PaymentClient

class ApiInstallService(private val client: PaymentClient, private val publicKey: String): InstallmentService {

    override fun allFor(paymentId: String, cardIssuerId: String): List<Installment> {
        return client.getInstallments(publicKey, paymentId, cardIssuerId)
                .execute()
                .body()!!.first()
                .installmentItems
                .map { it.toInstallment() }
    }

}
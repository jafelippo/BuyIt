package com.jaf.buyit.infrastructure.factory

import com.jaf.buyit.core.action.GetCardIssuersForPaymentMethod
import com.jaf.buyit.core.action.GetInstallments
import com.jaf.buyit.core.action.GetPaymentMethods
import com.jaf.buyit.core.domain.cardissuer.CardIssuerService
import com.jaf.buyit.core.domain.installment.InstallmentService
import com.jaf.buyit.core.domain.payment.PaymentMethodService
import com.jaf.buyit.infrastructure.PaymentClient
import com.jaf.buyit.infrastructure.cardissuer.ApiCardIssuerService
import com.jaf.buyit.infrastructure.installment.ApiInstallService
import com.jaf.buyit.infrastructure.payment.ApiPaymentMethods
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val API_URL = "https://api.mercadopago.com/v1/"
const val PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88"

class BuyItFactory {

    companion object {

        fun createGetInstallments(): GetInstallments {
            return GetInstallments(installmentService())
        }

        fun createGetPaymentMethods(): GetPaymentMethods{
            return GetPaymentMethods(paymentMethodService())
        }

        fun createGetCardIssuersForPaymentMethod(): GetCardIssuersForPaymentMethod {
            return GetCardIssuersForPaymentMethod(cardIssuerService())
        }

        private fun cardIssuerService(): CardIssuerService =
                ApiCardIssuerService(paymentClient(), PUBLIC_KEY)

        private fun paymentMethodService(): PaymentMethodService =
                ApiPaymentMethods(paymentClient(), PUBLIC_KEY)

        private fun installmentService(): InstallmentService =
                ApiInstallService(paymentClient(), PUBLIC_KEY)

        private fun paymentClient(): PaymentClient {
            return Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(PaymentClient::class.java)
        }
    }
}
package com.jaf.buyit.infrastructure.installment

import com.google.gson.annotations.SerializedName
import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.installment.Message


data class InstallmentItem(@SerializedName("recommended_message") private val recommendedMessage: String) {

    fun toInstallment(): Installment = Installment(Message(recommendedMessage))
}
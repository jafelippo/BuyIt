package com.jaf.buyit.infrastructure.cardissuer

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.cardissuer.ImageUrl
import com.jaf.buyit.core.domain.cardissuer.Name


data class CardIssuerItem(private val id: String, private val name: String, private val thumbnail: String) {

    fun toCardIssuer(): CardIssuer = CardIssuer(id, Name(name), ImageUrl(thumbnail))
}
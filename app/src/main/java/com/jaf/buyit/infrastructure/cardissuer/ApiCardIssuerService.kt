package com.jaf.buyit.infrastructure.cardissuer

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.cardissuer.CardIssuerService
import com.jaf.buyit.infrastructure.PaymentClient


class ApiCardIssuerService(private val client: PaymentClient, private val publicKey: String): CardIssuerService {

    override fun allFor(paymentId: String): List<CardIssuer> {
        return allCardIssuersFor(paymentId)
                .map { it.toCardIssuer() }
    }

    private fun allCardIssuersFor(paymentId: String): List<CardIssuerItem> =
            client.getCardIssuers(publicKey, paymentId).execute().body() ?: emptyList()
}
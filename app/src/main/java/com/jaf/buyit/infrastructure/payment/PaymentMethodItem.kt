package com.jaf.buyit.infrastructure.payment

import com.google.gson.annotations.SerializedName
import com.jaf.buyit.core.domain.payment.Name
import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.core.domain.payment.ImageUrl


data class PaymentMethodItem(private val id: String, private val name: String, private val thumbnail: String, @SerializedName("payment_type_id") private val paymentType: String) {

    fun isCreditCard(): Boolean = CREDIT_CARD_TYPE == paymentType

    fun toPaymentMethod(): PaymentMethod = PaymentMethod(id, Name(name), ImageUrl(thumbnail))

    companion object {
        const val CREDIT_CARD_TYPE = "credit_card"
    }
}
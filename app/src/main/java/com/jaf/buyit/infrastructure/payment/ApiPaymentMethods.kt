package com.jaf.buyit.infrastructure.payment

import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.core.domain.payment.PaymentMethodService
import com.jaf.buyit.infrastructure.PaymentClient


class ApiPaymentMethods(private val client: PaymentClient, private val publicKey: String): PaymentMethodService {

    override fun findAll(): List<PaymentMethod> {
        return getAllPaymentMethods()
                .filter { it.isCreditCard() }
                .map { it.toPaymentMethod() }
    }


    private fun getAllPaymentMethods():List<PaymentMethodItem> =
            client.getPayments(publicKey).execute().body() ?: emptyList()
}
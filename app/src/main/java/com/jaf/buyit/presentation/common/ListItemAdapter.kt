package com.jaf.buyit.presentation.common

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.jaf.buyit.R
import com.jaf.buyit.presentation.common.model.ItemViewModel
import kotlinx.android.synthetic.main.list_item.view.*


class ListItemAdapter(private val clickListener: ClickListener, private val items: List<ItemViewModel>): RecyclerView.Adapter<ListItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(itemView(parent))
    }

    private fun itemView(parent: ViewGroup?): View {
        return LayoutInflater.from(parent?.context)
                .inflate(R.layout.list_item, parent, false)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val item = items[position]
        holder?.name?.text = item.name
        holder?.itemView?.setOnClickListener { clickListener.onClick(position)}
        if (item.hasImage()) {
            Glide.with(holder?.image).load(item.imageUrl).into(holder?.image)
        }
    }

    override fun getItemCount(): Int = items.count()

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image = view.image
        val name = view.name
    }

    interface ClickListener {
        fun onClick(position: Int)
    }
}


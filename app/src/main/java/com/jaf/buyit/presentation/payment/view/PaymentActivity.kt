package com.jaf.buyit.presentation.payment.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jaf.buyit.R
import com.jaf.buyit.presentation.paymentmethod.view.PaymentMethodActivity
import kotlinx.android.synthetic.main.activity_payment.*
import android.text.Editable
import android.text.TextWatcher
import com.jaf.buyit.presentation.PaymentTransfer


class PaymentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        bindTextChangeListener()
        bindContinueClick()
    }

    private fun bindTextChangeListener() {
        moneyInput.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (s.isEmpty()) {
                    moneyInput?.setText(PLACEHOLDER)
                }
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }
        })
    }

    private fun bindContinueClick() {
        continueButton.setOnClickListener {
            if (isValidMoneyAmount()) {
                startActivity(paymentMethodIntent())
            } else {
                moneyInput.error = getString(R.string.amount_error)
            }
        }
    }

    private fun isValidMoneyAmount() = moneyInput.text.toString() != PLACEHOLDER

    private fun paymentMethodIntent() = PaymentMethodActivity.newIntent(this, PaymentTransfer(moneyAmount()))

    private fun moneyAmount() = Integer.parseInt(moneyInput.text.toString())

    companion object {
        const val PLACEHOLDER = "0"
    }
}

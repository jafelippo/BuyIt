package com.jaf.buyit.presentation.installment.factory

import com.jaf.buyit.infrastructure.factory.BuyItFactory
import com.jaf.buyit.presentation.installment.presenter.InstallmentPresenter
import com.jaf.buyit.presentation.installment.presenter.InstallmentView


class InstallmentFactory {

    companion object {

        fun createPresenter(view: InstallmentView, paymentId: String, cardIssuerId: String): InstallmentPresenter {
            return InstallmentPresenter(view, BuyItFactory.createGetInstallments(), paymentId, cardIssuerId)
        }
    }
}
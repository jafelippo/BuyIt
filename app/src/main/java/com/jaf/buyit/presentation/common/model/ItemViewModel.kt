package com.jaf.buyit.presentation.common.model

import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.core.domain.payment.PaymentMethod


data class ItemViewModel(val name: String, val imageUrl: String = "") {

    fun hasImage(): Boolean = imageUrl.isNotEmpty()

    companion object {

        fun from(model: PaymentMethod): ItemViewModel {
            return ItemViewModel(model.name().value, model.imageUrl().value)
        }

        fun from(model: CardIssuer): ItemViewModel {
            return ItemViewModel(model.name().value, model.imageUrl().value)
        }

        fun from(model: Installment): ItemViewModel {
            return ItemViewModel(model.message().value)
        }
    }
}
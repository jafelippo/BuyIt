package com.jaf.buyit.presentation.cardissuer.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.jaf.buyit.R
import com.jaf.buyit.presentation.PaymentTransfer
import com.jaf.buyit.presentation.cardissuer.factory.CardIssuerFactory
import com.jaf.buyit.presentation.cardissuer.presenter.CardIssuerPresenter
import com.jaf.buyit.presentation.cardissuer.presenter.CardIssuerView
import com.jaf.buyit.presentation.common.ListItemAdapter
import com.jaf.buyit.presentation.common.model.ItemViewModel
import com.jaf.buyit.presentation.installment.view.InstallmentActivity
import kotlinx.android.synthetic.main.activity_card_issuer.*

class CardIssuerActivity : AppCompatActivity(), CardIssuerView, ListItemAdapter.ClickListener {

    private lateinit var presenter: CardIssuerPresenter

    companion object {

        private const val ARGUMENT_PAYMENT_TRANSFER = "PAYMENT_TRANSFER"

        fun newIntent(context: Context, paymentTransfer: PaymentTransfer): Intent {
            val intent = Intent(context, CardIssuerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.putExtra(ARGUMENT_PAYMENT_TRANSFER, paymentTransfer)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_issuer)
        createPresenter()
        notifyViewCreation()
    }

    private fun notifyViewCreation() {
        presenter.onViewCreated()
    }

    private fun createPresenter() {
        presenter = CardIssuerFactory.createPresenter(this, paymentTransfer().paymentId)
    }

    private fun paymentTransfer() =
            intent.getSerializableExtra(ARGUMENT_PAYMENT_TRANSFER) as PaymentTransfer

    override fun showError() {
        Toast.makeText(this, "Ups ocurrió un error", Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        loader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loader.visibility = View.GONE
    }

    override fun showCardIssuers(items: List<ItemViewModel>) {
        with(itemList) {
            adapter = ListItemAdapter(this@CardIssuerActivity, items)
            layoutManager = android.support.v7.widget.LinearLayoutManager(this@CardIssuerActivity)
        }
    }

    override fun goToInstallmentWith(cardIssuerId: String) {
        startActivity(installmentActivityFor(cardIssuerId))
    }

    private fun installmentActivityFor(cardIssuerId: String) =
            InstallmentActivity.newIntent(this, paymentTransferWith(cardIssuerId))

    private fun paymentTransferWith(cardIssuerId: String): PaymentTransfer {
        val paymentTransfer = paymentTransfer()
        paymentTransfer.cardIssuerId = cardIssuerId
        return paymentTransfer
    }

    override fun onClick(position: Int) {
        presenter.cardIssuerSelectedAt(position)
    }

    override fun dismissWhenNoCardIssuers() {
        Toast.makeText(this, "No hay Bancos para el medio seleccionado", Toast.LENGTH_SHORT).show()
        finish()
    }
}

package com.jaf.buyit.presentation.paymentmethod.factory

import com.jaf.buyit.infrastructure.factory.BuyItFactory
import com.jaf.buyit.presentation.paymentmethod.presenter.PaymentMethodPresenter
import com.jaf.buyit.presentation.paymentmethod.presenter.PaymentMethodView


class PaymentMethodFactory {

    companion object {

        fun createPresenter(view: PaymentMethodView): PaymentMethodPresenter {
            return PaymentMethodPresenter(view, BuyItFactory.createGetPaymentMethods())
        }
    }
}
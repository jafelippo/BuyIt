package com.jaf.buyit.presentation.cardissuer.presenter

import com.jaf.buyit.presentation.common.model.ItemViewModel


interface CardIssuerView {
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun showCardIssuers(items: List<ItemViewModel>)
    fun goToInstallmentWith(cardIssuerId: String)
    fun dismissWhenNoCardIssuers()
}
package com.jaf.buyit.presentation.cardissuer.presenter

import com.jaf.buyit.core.action.GetCardIssuersForPaymentMethod
import com.jaf.buyit.core.domain.cardissuer.CardIssuer
import com.jaf.buyit.presentation.common.model.ItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class CardIssuerPresenter(private val view: CardIssuerView,
                          private val getCardIssuersForPaymentMethod: GetCardIssuersForPaymentMethod,
                          private val paymentId: String) {

    private lateinit var cardIssuers: List<CardIssuer>

    fun onViewCreated() {
        view.showLoading()
        requestPaymentMethods()
    }

    private fun requestPaymentMethods() {
        getCardIssuersForPaymentMethod.build(paymentId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onCardIssuers(it) }, { onError() })
    }

    private fun onCardIssuers(cardIssuers: List<CardIssuer>) {
        view.hideLoading()
        this.cardIssuers = cardIssuers
        if (hasCardIssuers(cardIssuers)) {
            view.showCardIssuers(cardIssuers.map { ItemViewModel.from(it) })
        } else {
            view.dismissWhenNoCardIssuers()
        }
    }

    private fun hasCardIssuers(cardIssuers: List<CardIssuer>) =
            cardIssuers.size > 0

    private fun onError() {
        view.hideLoading()
        view.showError()
    }

    fun cardIssuerSelectedAt(position: Int) {
        view.goToInstallmentWith(cardIssuerIdAt(position))
    }

    private fun cardIssuerIdAt(position: Int) = cardIssuers[position].identifier()
}
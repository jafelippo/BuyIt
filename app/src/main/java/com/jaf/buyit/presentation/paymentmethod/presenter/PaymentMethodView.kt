package com.jaf.buyit.presentation.paymentmethod.presenter

import com.jaf.buyit.presentation.common.model.ItemViewModel


interface PaymentMethodView {
    fun showError()
    fun showLoading()
    fun hideLoading()
    fun showPaymentMethods(itemMethods: List<ItemViewModel>)
    fun goToCardIssuerWithPaymentMethod(paymentMethod: String)
}
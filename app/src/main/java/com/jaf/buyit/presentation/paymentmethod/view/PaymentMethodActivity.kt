package com.jaf.buyit.presentation.paymentmethod.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.jaf.buyit.R
import com.jaf.buyit.presentation.PaymentTransfer
import com.jaf.buyit.presentation.cardissuer.view.CardIssuerActivity
import com.jaf.buyit.presentation.common.ListItemAdapter
import com.jaf.buyit.presentation.paymentmethod.factory.PaymentMethodFactory
import com.jaf.buyit.presentation.common.model.ItemViewModel
import com.jaf.buyit.presentation.paymentmethod.presenter.PaymentMethodPresenter
import com.jaf.buyit.presentation.paymentmethod.presenter.PaymentMethodView
import kotlinx.android.synthetic.main.activity_card_issuer.*

class PaymentMethodActivity : AppCompatActivity(), PaymentMethodView, ListItemAdapter.ClickListener {

    private lateinit var presenter: PaymentMethodPresenter

    companion object {

        private const val ARGUMENT_PAYMENT_TRANSFER = "PAYMENT_TRANSFER"

        fun newIntent(context: Context, paymentTransfer: PaymentTransfer): Intent {
            val intent = Intent(context, PaymentMethodActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.putExtra(ARGUMENT_PAYMENT_TRANSFER, paymentTransfer)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)
        createPresenter()
        notifyViewCreation()
    }

    private fun notifyViewCreation() {
        presenter.onViewCreated()
    }

    private fun createPresenter() {
        presenter = PaymentMethodFactory.createPresenter(this)
    }

    override fun showError() {
        Toast.makeText(this, "Ups ocurrió un error", Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        loader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loader.visibility = View.GONE
    }

    override fun showPaymentMethods(itemMethods: List<ItemViewModel>) {
        with(itemList) {
            adapter = ListItemAdapter(this@PaymentMethodActivity, itemMethods)
            layoutManager = LinearLayoutManager(this@PaymentMethodActivity)
        }
    }

    override fun onClick(position: Int) {
        presenter.paymentSelectedAt(position)
    }

    override fun goToCardIssuerWithPaymentMethod(paymentMethod: String) {
        startActivity(cardIssuerActivityFor(paymentMethod))
    }

    private fun cardIssuerActivityFor(paymentMethod: String) =
            CardIssuerActivity.newIntent(this, paymentTransferWith(paymentMethod))

    private fun paymentTransferWith(paymentMethod: String): PaymentTransfer {
        val paymentTransfer = paymentTransfer()
        paymentTransfer.paymentId = paymentMethod
        return paymentTransfer
    }

    private fun paymentTransfer() =
            intent.getSerializableExtra(ARGUMENT_PAYMENT_TRANSFER) as PaymentTransfer
}

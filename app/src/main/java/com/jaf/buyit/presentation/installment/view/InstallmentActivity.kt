package com.jaf.buyit.presentation.installment.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.jaf.buyit.R
import com.jaf.buyit.presentation.PaymentTransfer
import com.jaf.buyit.presentation.common.ListItemAdapter
import com.jaf.buyit.presentation.common.model.ItemViewModel
import com.jaf.buyit.presentation.installment.factory.InstallmentFactory
import com.jaf.buyit.presentation.installment.presenter.InstallmentPresenter
import com.jaf.buyit.presentation.installment.presenter.InstallmentView
import com.jaf.buyit.presentation.payment.view.PaymentActivity
import kotlinx.android.synthetic.main.activity_card_issuer.*

class InstallmentActivity : AppCompatActivity(), InstallmentView, ListItemAdapter.ClickListener {

    private lateinit var presenter: InstallmentPresenter

    companion object {

        private const val ARGUMENT_PAYMENT_TRANSFER = "PAYMENT_TRANSFER"

        fun newIntent(context: Context, paymentTransfer: PaymentTransfer): Intent {
            val intent = Intent(context, InstallmentActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            intent.putExtra(ARGUMENT_PAYMENT_TRANSFER, paymentTransfer)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment)
        createPresenter()
        notifyViewCreation()
    }

    private fun notifyViewCreation() {
        presenter.onViewCreated()
    }

    private fun createPresenter() {
        presenter = InstallmentFactory.createPresenter(this, paymentTransfer().paymentId, paymentTransfer().cardIssuerId)
    }

    private fun paymentTransfer() =
            intent.getSerializableExtra(ARGUMENT_PAYMENT_TRANSFER) as PaymentTransfer

    override fun showError() {
        Toast.makeText(this, "Ups ocurrió un error", Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        loader.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loader.visibility = View.GONE
    }

    override fun showInstallments(items: List<ItemViewModel>) {
        with(itemList) {
            adapter = ListItemAdapter(this@InstallmentActivity, items)
            layoutManager = android.support.v7.widget.LinearLayoutManager(this@InstallmentActivity)
        }
    }

    override fun showPaymentInfo(info: String) {
        Toast.makeText(this, info, Toast.LENGTH_SHORT).show()
    }

    override fun close() {
        startActivity(Intent(this, PaymentActivity::class.java))
    }

    override fun onClick(position: Int) {
        presenter.installmentSelectedAt(position)
    }
}

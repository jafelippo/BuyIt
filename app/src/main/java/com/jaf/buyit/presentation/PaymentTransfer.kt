package com.jaf.buyit.presentation
import java.io.Serializable


data class PaymentTransfer(var amount: Int,
                      var paymentId: String = "",
                      var cardIssuerId: String = ""): Serializable
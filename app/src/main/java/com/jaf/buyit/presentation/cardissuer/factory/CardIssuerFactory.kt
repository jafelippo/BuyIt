package com.jaf.buyit.presentation.cardissuer.factory

import com.jaf.buyit.infrastructure.factory.BuyItFactory
import com.jaf.buyit.presentation.cardissuer.presenter.CardIssuerPresenter
import com.jaf.buyit.presentation.cardissuer.presenter.CardIssuerView


class CardIssuerFactory {

    companion object {

        fun createPresenter(view: CardIssuerView, paymentId: String): CardIssuerPresenter {
            return CardIssuerPresenter(view, BuyItFactory.createGetCardIssuersForPaymentMethod(), paymentId)
        }
    }
}
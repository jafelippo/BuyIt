package com.jaf.buyit.presentation.installment.presenter

import com.jaf.buyit.presentation.common.model.ItemViewModel


interface InstallmentView {
    fun showLoading()
    fun hideLoading()
    fun showError()
    fun showInstallments(items: List<ItemViewModel>)
    fun showPaymentInfo(info: String)
    fun close()
}
package com.jaf.buyit.presentation.installment.presenter

import com.jaf.buyit.core.action.GetInstallments
import com.jaf.buyit.core.domain.installment.Installment
import com.jaf.buyit.presentation.common.model.ItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class InstallmentPresenter(private val view: InstallmentView,
                           private val getInstallments: GetInstallments,
                           paymentId: String,
                           cardIssuerId: String) {

    private val actionData = GetInstallments.ActionData(paymentId, cardIssuerId)
    private lateinit var installments: List<Installment>

    fun onViewCreated() {
        view.showLoading()
        requestPaymentMethods()
    }

    private fun requestPaymentMethods() {
        getInstallments.build(actionData)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onInstallments(it) }, { onError() })
    }

    private fun onInstallments(installments: List<Installment>) {
        view.hideLoading()
        this.installments = installments
        view.showInstallments(installments.map { ItemViewModel.from(it) })
    }

    private fun onError() {
        view.hideLoading()
        view.showError()
    }

    fun installmentSelectedAt(position: Int) {
        view.showPaymentInfo(installments[position].message().value)
        view.close()
    }
}
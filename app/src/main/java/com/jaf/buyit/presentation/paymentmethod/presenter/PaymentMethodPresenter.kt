package com.jaf.buyit.presentation.paymentmethod.presenter

import com.jaf.buyit.core.action.GetPaymentMethods
import com.jaf.buyit.core.domain.payment.PaymentMethod
import com.jaf.buyit.presentation.common.model.ItemViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PaymentMethodPresenter(private val view: PaymentMethodView, private val getPaymentMethods: GetPaymentMethods) {

    private lateinit var paymentMethods: List<PaymentMethod>

    fun onViewCreated() {
        view.showLoading()
        requestPaymentMethods()
    }

    private fun requestPaymentMethods() {
        getPaymentMethods.build()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ onPayments(it) }, { onError() })
    }

    private fun onPayments(paymentMethods: List<PaymentMethod>) {
        view.hideLoading()
        this.paymentMethods = paymentMethods
        view.showPaymentMethods(paymentMethods.map { ItemViewModel.from(it) })
    }

    private fun onError() {
        view.hideLoading()
        view.showError()
    }

    fun paymentSelectedAt(position: Int) {
        view.goToCardIssuerWithPaymentMethod(paymentIdAt(position))
    }

    private fun paymentIdAt(position: Int) = paymentMethods[position].identifier()
}